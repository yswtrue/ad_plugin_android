# Ad Plugin Android


## START USAGE

#### gradle

project build.gradle

```gradle
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.com/api/v4/projects/45236205/packages/maven"
        }
        ...
    }
}
```

app build.gradle

```gradle
android {
    ...
    packagingOptions {
        exclude("META-INF/INDEX.LIST")
    }
    ...
}
dependencies {
    ...
    implementation "ads.kingpoint.plugins:android:<latest-version>"
    ...
}
```

#### Java

```java

```